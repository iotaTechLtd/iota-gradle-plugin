import java.io.FileInputStream
import java.util.*

val PLUGIN_VERSION: String by project

gradlePlugin {
    plugins {
        create("iotaPlugin") {
            id = "uk.co.iotatech.gradle"
            displayName = "Iotatech Gradle Plugin"
            description = "Iotatech internal gradle plugin to unify configuration and simplify individual project settings. Should be used in build.gradle file."
            implementationClass = "uk.co.iotatech.gradle.IotatechCorePlugin"
        }

        create("iotaServicePlugin") {
            id = "uk.co.iotatech.gradle.service"
            displayName = "Iotatech Gradle Service Plugin"
            description = "Iotatech plugin to be applied to a microservice, aggregates number of other plugins in a single unit"
            implementationClass = "uk.co.iotatech.gradle.IotatechServicePlugin"
        }

        create("iotaBootPlugin") {
            id = "uk.co.iotatech.gradle.boot"
            displayName = "Iotatech Gradle Spring Boot Plugin"
            description = "Iotatech plugin to configure gradle spring boot plugin"
            implementationClass = "uk.co.iotatech.gradle.IotatechBootPlugin"
        }

        create("iotaDetektPlugin") {
            id = "uk.co.iotatech.gradle.detekt"
            displayName = "Iotatech Gradle Detekt Plugin"
            description = "Iotatech plugin to configure gradle detekt plugin"
            implementationClass = "uk.co.iotatech.gradle.IotatechDetektPlugin"
        }

        create("iotaKtlintPlugin") {
            id = "uk.co.iotatech.gradle.ktlint"
            displayName = "Iotatech Gradle Ktlint Plugin"
            description = "Iotatech plugin to configure gradle ktlint plugin"
            implementationClass = "uk.co.iotatech.gradle.IotatechKtlintPlugin"
        }

        create("iotaSonarPlugin") {
            id = "uk.co.iotatech.gradle.sonar"
            displayName = "Iotatech Gradle Sonar Plugin"
            description = "Iotatech plugin to configure gradle sonar plugin"
            implementationClass = "uk.co.iotatech.gradle.IotatechSonarPlugin"
        }

        create("iotaJacocoPlugin") {
            id = "uk.co.iotatech.gradle.jacoco"
            displayName = "Iotatech Gradle Jacoco Plugin"
            description = "Iotatech plugin to configure gradle jacoco plugin"
            implementationClass = "uk.co.iotatech.gradle.IotatechJacocoPlugin"
        }

        create("iotaVersioningPlugin") {
            id = "uk.co.iotatech.gradle.versioning"
            displayName = "Iotatech Gradle Versioning Plugin"
            description = "Iotatech plugin to configure gradle versioning plugin"
            implementationClass = "uk.co.iotatech.gradle.IotatechVersioningPlugin"
        }

        create("iotaPublishingPlugin") {
            id = "uk.co.iotatech.gradle.publishing"
            displayName = "Iotatech Gradle Publishing Plugin"
            description = "Iotatech plugin to configure maven package publish plugin"
            implementationClass = "uk.co.iotatech.gradle.IotatechPublishingPlugin"
        }

        create("iotaSettingsPlugin") {
            id = "uk.co.iotatech.gradle.settings"
            displayName = "Iotatech Gradle Settings Plugin"
            description = "Iotatech internal gradle plugin to configure gradle settings. Should be used in settings.gradle file."
            implementationClass = "uk.co.iotatech.gradle.IotatechSettingsPlugin"
        }
    }
}

pluginBundle {
    website = "https://bitbucket.org/iotaTechLtd/iota-gradle-plugin/src/master/README.md"
    vcsUrl = "https://bitbucket.org/iotaTechLtd/iota-gradle-plugin.git"
    tags = listOf("iotatech", "kotlin", "detekt", "ktlint", "spring", "jacoco")
}

publishing {
    repositories {
        mavenLocal()
    }
}

plugins {
    kotlin("jvm") version "1.6.21"
    id("org.gradle.kotlin.kotlin-dsl") version "2.1.7"
    id("com.gradle.plugin-publish") version "0.19.0"
    id("java-gradle-plugin")
    id("maven-publish")
    id("java")
}

buildscript {
    repositories {
        maven("https://repo.spring.io/libs-release")
        mavenCentral()
        gradlePluginPortal()
    }
}

group = "uk.co.iotatech"
version = PLUGIN_VERSION

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    val props = Properties().apply {
        load(FileInputStream("src/main/resources/versions.properties"))
    }

    val dependencyManagementVersion = props.getProperty("dependencyManagementVersion")
    val sonarqubeVersion = props.getProperty("sonarqubeVersion")
    val detektVersion = props.getProperty("detektVersion")
    val bootVersion = props.getProperty("bootVersion")
    val kotlinVersion = props.getProperty("kotlinVersion")

    implementation(kotlin("stdlib"))
    implementation(gradleApi())

    implementation("io.spring.gradle:dependency-management-plugin:$dependencyManagementVersion")
    implementation("org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:$sonarqubeVersion")
    implementation("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:$detektVersion")
    implementation("org.springframework.boot:spring-boot-gradle-plugin:$bootVersion")

    api("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
    api("org.jetbrains.kotlin:kotlin-allopen:$kotlinVersion")
    api("org.jetbrains.kotlin:kotlin-noarg:$kotlinVersion")
}

tasks.create("patch") {
    group = "versioning"
    doLast {
        val PLUGIN_VERSION: String by project

        val currentPluginVersion = PLUGIN_VERSION.split(".").toMutableList()
        val currentPatchVersion = currentPluginVersion.last()
        currentPluginVersion[2] = currentPatchVersion.toInt().plus(1).toString()

        ant.withGroovyBuilder {
            "propertyfile"("file" to "gradle.properties") {
                "entry"("key" to "PLUGIN_VERSION", "value" to currentPluginVersion.joinToString(separator = "."))
            }
        }
    }
}

tasks.create("minor") {
    group = "versioning"
    doLast {
        val PLUGIN_VERSION: String by project

        val currentPluginVersion = PLUGIN_VERSION.split(".").toMutableList()
        currentPluginVersion[2] = "0"
        currentPluginVersion[1] = currentPluginVersion[1].toInt().plus(1).toString()

        ant.withGroovyBuilder {
            "propertyfile"("file" to "gradle.properties") {
                "entry"("key" to "PLUGIN_VERSION", "value" to currentPluginVersion.joinToString(separator = "."))
            }
        }
    }
}

tasks.create("major") {
    group = "versioning"
    doLast {
        val PLUGIN_VERSION: String by project

        val currentPluginVersion = PLUGIN_VERSION.split(".").toMutableList()
        currentPluginVersion[1] = "0"
        currentPluginVersion[2] = "0"
        currentPluginVersion[0] = currentPluginVersion[0].toInt().plus(1).toString()

        ant.withGroovyBuilder {
            "propertyfile"("file" to "gradle.properties") {
                "entry"("key" to "PLUGIN_VERSION", "value" to currentPluginVersion.joinToString(separator = "."))
            }
        }
    }
}