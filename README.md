## Iotatech Gradle Plugin

Iotatech plugin idea is to simplify individual project configuration by collecting all defaults and best practices in
a single place that is easy to manage and support. Second goal is to simplify process of updates for new versions of
gradle, spring, jacoco, detekt and other supporting libraries or frameworks.

Iotatech plugins aggregate configuration for the following third party plugins:

| Library                      | Version        |
|------------------------------|----------------|
| gradle                       | 7.4.2          |
| detekt                       | 1.20.0         |
| ktlint                       | 0.45.2         |
| sonar                        | 3.3            |
| jacoco                       | 0.8.7          |
| versioning                   | -              |
| spring dependency management | 1.0.11.RELEASE |
| spring boot                  | 2.7.3          |
| spring cloud                 | 2021.0.4       |
| kotlin                       | 1.6.21         |
| maven publish                | 0.19.0         |

Also support for boilerplate configuration is included:

1. Repositories
2. Gradle Wrapper
3. Gradle Build and Cache Configurations

### ♻️ Change Log

| Version | Change                                                                                               | Author         | Datetime   |
|---------|------------------------------------------------------------------------------------------------------|----------------|------------|
| 0.0.17  | Added support to provide versions of spring boot and cloud, updated documentation                    | Ilya Katlinski | 06.01.2022 |
| 0.0.18  | Added support for gradle settings plugin with build cache support - `uk.co.iotatech.gradle.settings` | Ilya Katlinski | 06.01.2022 |
| 0.0.20  | Made a split of a single plugin into multiple ones for better flexibility, not a final version       | Ilya Katlinski | 01.02.2022 |
| 0.0.21  | Update jacoco reports generation to remove deprecated code                                           | Ilya Katlinski | 03.02.2022 |
| 0.1.0   | First stable release of the plugin                                                                   | Ilya Katlinski | 03.03.2022 |
| 0.1.1   | Update jacoco test reports merge process to work correctly                                           | Ilya Katlinski | 15.03.2022 |
| 0.2.0   | Exclude kotlin kapt plugin form the core as it is too specific                                       | Ilya Katlinski | 08.04.2022 |
| 0.2.1   | Exclude test and generated sources from ktlint checks                                                | Ilya Katlinski | 11.04.2022 |
| 0.2.2   | Add freeCompilerArgs parameter to kotlin settings                                                    | Ilya Katlinski | 11.04.2022 |
| 0.2.3   | Add activation package to jacoco and sonar exclusions                                                | Ilya Katlinski | 13.04.2022 |
| 0.2.4   | Add kotlin no-arg and all-open plugins to core                                                       | Ilya Katlinski | 14.04.2022 |
| 0.2.5   | Add makeModuleVersionSnapshot task to publishing plugin                                              | Ilya Katlinski | 14.04.2022 |
| 0.2.6   | Add override of kotlin version in boot and cloud by default                                          | Ilya Katlinski | 16.04.2022 |
| 0.2.7   | Add kotlin to default dependencies of the project                                                    | Ilya Katlinski | 16.04.2022 |
| 0.2.8   | Allow to override kotlin version                                                                     | Ilya Katlinski | 16.04.2022 |
| 0.2.9   | Remove kotlin api dependencies and leave only implementation and test implementation                 | Ilya Katlinski | 16.04.2022 |
| 0.2.10  | Remove exact kotlin version from dependencies as it is provided by spring boms                       | Ilya Katlinski | 16.04.2022 |
| 0.2.11  | Add detekt cli dependency to detekt configuration                                                    | Ilya Katlinski | 18.04.2022 |
| 0.3.0   | Update dependencies: kotlin - 1.6.21, boot - 2.6.7, cloud - 2021.0.1, gradle - 7.4.2                 | Ilya Katlinski | 19.04.2022 |
| 0.3.1   | Update dependencies: cloud - 2021.0.2                                                                | Ilya Katlinski | 29.04.2022 |
| 0.4.0   | Update dependencies: boot - 2.7.0                                                                    | Maxim Yesman   | 23.05.2022 |
| 0.5.0   | Update dependencies: cloud - 2021.0.3, kotlin 1.7.0                                                  | Maxim Yesman   | 13.06.2022 |
| 0.5.1   | Add reproducible jar config                                                                          | Maxim Yesman   | 13.06.2022 |
| 0.5.2   | Revert dependencies: kotlin 1.6.21                                                                   | Maxim Yesman   | 14.06.2022 |
| 0.5.3   | Fix reproducible jar config                                                                          | Maxim Yesman   | 21.06.2022 |
| 0.5.4   | Update dependencies: boot - 2.7.1                                                                    | Maxim Yesman   | 21.06.2022 |
| 0.5.5   | Update dependencies: boot - 2.7.2                                                                    | Maxim Yesman   | 22.07.2022 |
| 0.5.6   | Update dependencies: boot - 2.7.3                                                                    | Maxim Yesman   | 09.09.2022 |
| 0.5.7   | Update dependencies: cloud - 2021.0.4                                                                | Maxim Yesman   | 09.09.2022 |


### 🚀 Usage

1. Add iotatech plugin version to project `gradle.properties` file

```text
...
iotatechPluginVersion=<VERSION>
...
```

2. In case you want to add gradle settings plugin update `settings.gradle`

```groovy
buildscript {
    repositories {
        gradlePluginPortal()
    }

    dependencies {
        classpath "uk.co.iotatech:iota-gradle-plugin:$iotatechPluginVersion"
    }
}

pluginManager.apply("PLUGIN_ID")
```

3. In case you want to add gradle build plugin update `build.gradle`

```groovy
buildscript {
    repositories {
        gradlePluginPortal()
    }

    dependencies {
        classpath "uk.co.iotatech:iota-gradle-plugin:$iotatechPluginVersion"
    }
}

apply plugin: "PLUGIN_ID"
```

💡️ Please check versions usage rules above

### 🔧 Configuration

Build plugins are configured in a `build.gradle` via DSL by enclosing configuration in `iotatech` closure:

```groovy
iotatech {
    // configuration of the plugins
}
```

Settings plugins are configured in a `settings.gradle` via DSL by enclosing configuration in `iotatechSettings` closure:

```groovy
iotatechSettings {
    // configuration of the plugins
}
```

### 📦 Plugins

#### Core Build Plugin

🔗 Plugin link: https://plugins.gradle.org/plugin/uk.co.iotatech.gradle

🆔 Plugin Id: `uk.co.iotatech.gradle`

##### Kotlin

ℹ️ Allows overriding kotlin build options by providing jvm build target.

Configuration settings

| Setting            | Type        | Description                                         | Default Value          |
|--------------------|-------------|-----------------------------------------------------|------------------------|
| jvmTarget          | JavaVersion | Version of the jvm to which code should be compiled | JavaVersion.VERSION_17 |
| freeCompilerArgs   | string list | Allows to provide custom kotlin compiler arguments  | []                     |

Configuration example
```groovy
iotatech {
    kotlin {
        jvmTarget.set(JavaVersion.VERSION_11)
    }
}
```

##### Repositories

ℹ️ Automatically configures following repositories for the project:

1. local maven
2. iotatech maven (s3)
3. maven central
4. gradle plugins portal

Iotatech maven repository requires following configuration for local development in a form of environment variables:

| Setting                          | Type     | Description                                     | Default Value |
|----------------------------------|----------|-------------------------------------------------|---------------|
| ITC_MAVEN_AWS_S3_REPO_URL *      | string   | Url for the iotatech s3 repository              |               |
| ITC_MAVEN_AWS_S3_REPO_USERNAME * | string   | AWS IAM user name to be used to access repo     |               |
| ITC_MAVEN_AWS_S3_REPO_PASSWORD * | string   | AWS IAM user password to be used to access repo |               |

Iotatech maven repository requires following configuration for ci configuration in a form of environment variables:

| Setting                     | Type       | Description                        | Default Value |
|-----------------------------|------------|------------------------------------|---------------|
| ITC_MAVEN_AWS_S3_REPO_URL * | string     | Url for the iotatech s3 repository |               |
| AWS_ACCESS_KEY_ID *         | string     | AWS IAM user access key            |               |
| AWS_SECRET_ACCESS_KEY *     | string     | AWS IAM user secret key            |               |
| AWS_SESSION_TOKEN *         | string     | AWS IAM user session id            |               |

##### Gradle

ℹ️ Allows configuring version of the gradle wrapper to be used by the project.

Configuration settings

| Setting | Type   | Description                  | Default Value |
|---------|--------|------------------------------|---------------|
| version | string | Version of the gradle to use | 7.3.1         |

Configuration example
```groovy
iotatech {
    gradle {
        version.set("8.0.0")
    }
}
```

##### Gradle Build Configurations

ℹ️ Allows configuring project global build configurations like:
1. local cache strategies
2. dependency exclusions

Configuration settings

| Setting   | Type    | Description                                             | Default Value |
|-----------|---------|---------------------------------------------------------|---------------|
| enabled   | boolean | Flag to define if build configuration is enabled or not | true          |
| cacheTtl  | int     | TTL of the build cache                                  | 4             |
| cacheUnit | string  | Unit of the TTL build cache                             | "hours"       |

Configuration example
```groovy
iotatech {
    buildConfigurations {
        enabled.set(true)
        cacheTtl.set(1)
        cacheUnit.set("hours")
    }
}
```
##### Spring Dependency Management

ℹ️ Configures spring boot and cloud BOMs and allows overriding versions of libraries provided by those BOMs.

Configuration settings

| Setting         | Type    | Description                                                     | Default Value      |
|-----------------|---------|-----------------------------------------------------------------|--------------------|
| enabled         | boolean | Flag to define if plugin is enabled or not                      | true               |
| kotlinVersion   | string  | Version of the kotlin to use                                    | 1.6.21             |
| boot.version    | string  | Version of spring boot bom to use as a basis for dependencies   | 2.7.3              |
| boot.overrides  | map     | Map of dependency to version to be replaces in spring boot bom  | ["kotlin.version"] |
| cloud.version   | string  | Version of spring cloud bom to use as a basis for dependencies  | 2021.0.2           |
| cloud.overrides | map     | Map of dependency to version to be replaces in spring cloud bom | ["kotlin.version"] |

Configuration example
```groovy
iotatech {
    dependencyManagement {
        enabled.set(true)
        kotlinVersion.set("1.6.20")
        
        boot {
            version.set("2.6.6")
            overrides.set([
                    "aws-java-sdk.version": "11.2.3",
                    "liquibase.version"   : "4.6.2",
            ])
        }
        cloud {
            version.set("2021.0.2")
            overrides.set([
                    "aws-java-sdk.version": "11.2.3",
            ])
        }
    }
}
```

#### Service Build Plugin

🔗 Plugin link: https://plugins.gradle.org/plugin/uk.co.iotatech.gradle.service

🆔 Plugin Id: `uk.co.iotatech.gradle.service`

ℹ️ Plugin to be used for a microservice projects, simply aggregates core plugin with other pluggable plugins. Used to
simplify configuration of each particular service to reduce development efforts.

Includes:

1. `uk.co.iotatech.gradle`
1. `uk.co.iotatech.gradle.detekt`
1. `uk.co.iotatech.gradle.ktlint`
1. `uk.co.iotatech.gradle.jacoco`
1. `uk.co.iotatech.gradle.sonar`
1. `uk.co.iotatech.gradle.boot`
1. `uk.co.iotatech.gradle.versioning`

#### Detekt Plugin

🔗 Plugin link: https://plugins.gradle.org/plugin/uk.co.iotatech.gradle.detekt

🆔 Plugin Id: `uk.co.iotatech.gradle.detekt`

ℹ️ Plugin configures build jobs to run detekt checks and produce reports as result of the execution. Automatically runs
during build process of the project if enabled.

Configuration settings

| Setting | Type        | Description                                                               | Default Value |
|---------|-------------|---------------------------------------------------------------------------|---------------|
| version | string      | Version of the detekt plugin to use                                       | 1.20.0        |
| source  | string list | List of paths to directories that should be checked by detekt             | src/main      |
| config  | string      | String path to the detekt configuration file relative to the project root | detekt.yml    |

Configuration example
```groovy
iotatech {
    detekt {
        version.set("1.19.0")
        source.set(["src/main"])
        config.set("detekt.yml")
    }
}
```

#### Ktlint

🔗 Plugin link: https://plugins.gradle.org/plugin/uk.co.iotatech.gradle.ktlint

🆔 Plugin Id: `uk.co.iotatech.gradle.ktlint`

ℹ️ Plugin configures build jobs to run ktlint checks and produce reports as result of the execution. Automatically runs
during build process of the project if enabled.

Configuration settings

| Setting | Type        | Description                                | Default Value |
|---------|-------------|--------------------------------------------|---------------|
| enabled | boolean     | Flag to define if plugin is enabled or not | true          |
| version | string      | Version of the ktlint plugin to use        | 0.45.2         |

Configuration example
```groovy
iotatech {
    ktlint {
        enabled.set(true)
        version.set("0.43.2")
    }
}
```

#### Jacoco Plugin

🔗 Plugin link: https://plugins.gradle.org/plugin/uk.co.iotatech.gradle.jacoco

🆔 Plugin Id: `uk.co.iotatech.gradle.jacoco`

ℹ️ Integrates jacoco test coverage reports into the project, aggregates multiple test tasks reports into single report, 
allows excluding of packages or classes from the report.

Configuration settings

| Setting              | Type            | Description                                                     | Default Value |
|----------------------|-----------------|-----------------------------------------------------------------|---------------|
| enabled              | boolean         | Flag to define if plugin is enabled or not                      | true          |
| version              | string          | Version of the jacoco plugin to use                             | 0.8.7         |
| exclusions           | list of strings | List of ant format string paths to files or packages to exclude | []            |
| reports.htmlEnabled  | boolean         | Flag to define if html reports should be generated              | false         |
| reports.xmlEnabled   | boolean         | Flag to define if xml reports should be generated               | true          |

Configuration example
```groovy
iotatech {
    jacoco {
        enabled.set(true)
        version.set("0.8.7")
        exclusions.set(["**/**Dto.*"])
        
        reports {
            htmlEnabled.set(true)
            xmlEnabled.set(false)
        }
    }
}
```

#### Sonar Plugin

🔗 Plugin link: https://plugins.gradle.org/plugin/uk.co.iotatech.gradle.sonar

🆔 Plugin Id: `uk.co.iotatech.gradle.sonar`

ℹ️ Sonarqube server plugin configuration, if enabled requires project name and key to be configured for the service.
Is not executed automatically, but is used in CI jobs to generate weekly or bi-weekly reports on projects.

Configuration settings

| Setting       | Type    | Description                                  | Default Value |
|---------------|---------|----------------------------------------------|---------------|
| enabled       | boolean | Flag to define if plugin is enabled or not   | true          |
| projectName * | string  | Name of the project in sonar server instance |               |
| projectKey *  | string  | Key of the project in sonar server instance  |               |

Configuration example
```groovy
iotatech {
    sonar {
        enabled.set(true)
        projectName.set("CLM-ADMIN-SERVICE")
        projectKey.set("uk.co.iotatech:clm-admin-service")
    }
}
```

#### Versioning Plugin

🔗 Plugin link: https://plugins.gradle.org/plugin/uk.co.iotatech.gradle.versioning

🆔 Plugin Id: `uk.co.iotatech.gradle.versioning`

ℹ️ Provides gradle tasks to update version of the project (stored as MODULE_VERSION in gradle.properties):

1. `increaseModuleBuildVersion` - updates patch version of the project based on teamcity `builNumber` property
2. `increaseModuleMinorVersion` - increments minor version of the project, also sets patch version to `builNumber` property
if provided
3. `increaseModuleBuildVersion` - increments major version of the project, also sets patch version to `builNumber` property 
if provided and minor version to 0

Configuration settings

| Setting       | Type            | Description                                  | Default Value |
|---------------|-----------------|----------------------------------------------|---------------|
| enabled       | boolean         | Flag to define if plugin is enabled or not   | true          |

Configuration example
```groovy
iotatech {
    versioning {
        enabled.set(true)
    }
}
```

#### Maven Publishing Plugin

🔗 Plugin link: https://plugins.gradle.org/plugin/uk.co.iotatech.gradle.publishing

🆔 Plugin Id: `uk.co.iotatech.gradle.publishing`

ℹ️ Wrapper around default maven-publish plugin to configure publishing of the artifacts to aws s3 bucket

Configuration settings

| Setting       | Type            | Description                                  | Default Value |
|---------------|-----------------|----------------------------------------------|---------------|
| enabled       | boolean         | Flag to define if plugin is enabled or not   | true          |

In addition, uses number of the environment variables to connect to s3 bucket

Configuration for local environment

| Setting                             | Type     | Description                                       | Default Value |
|-------------------------------------|----------|---------------------------------------------------|---------------|
| ITC_MAVEN_AWS_S3_REPO_PUBLISH_URL * | string   | Url for the iotatech s3 repository for publishing |               |
| ITC_MAVEN_AWS_S3_REPO_USERNAME *    | string   | AWS IAM user name to be used to access repo       |               |
| ITC_MAVEN_AWS_S3_REPO_PASSWORD *    | string   | AWS IAM user password to be used to access repo   |               |

Configuration for CI

| Setting                             | Type       | Description                                       | Default Value |
|-------------------------------------|------------|---------------------------------------------------|---------------|
| ITC_MAVEN_AWS_S3_REPO_PUBLISH_URL * | string     | Url for the iotatech s3 repository for publishing |               |
| AWS_ACCESS_KEY_ID *                 | string     | AWS IAM user access key                           |               |
| AWS_SECRET_ACCESS_KEY *             | string     | AWS IAM user secret key                           |               |
| AWS_SESSION_TOKEN *                 | string     | AWS IAM user session id                           |               |

Configuration example
```groovy
iotatech {
    publishing {
        enabled.set(true)
    }
}
```

#### Spring Boot Plugin

🔗 Plugin link: https://plugins.gradle.org/plugin/uk.co.iotatech.gradle.boot

🆔 Plugin Id: `uk.co.iotatech.gradle.boot`

ℹ️ Enables spring boot plugin to provide `buildInfo()` with the project jar.

ℹ️ Currently, no configuration settings is available for this plugin

#### Build Settings Plugin

🔗 Plugin link: https://plugins.gradle.org/plugin/uk.co.iotatech.gradle.settings

🆔 Plugin Id: `uk.co.iotatech.gradle.settings`

##### Build Cache

Configuration settings

| Setting   | Type    | Description                                     | Default Value |
|-----------|---------|-------------------------------------------------|---------------|
| enabled   | boolean | Flag to define if build cache is enabled or not | true          |

In addition to configuration settings build cache plugin relies on a number of the environment variables to operate

| Setting                     | Type     | Description                                                                                  | Default Value |
|-----------------------------|----------|----------------------------------------------------------------------------------------------|---------------|
| GRADLE_CACHE_ENABLED        | string   | Additional flag to define is cache fill be enabled, used by CI, expected `yes` to be enabled |               |
| GRADLE_CACHE_USER *         | string   | Gradle cache user to authenticate the request, required if GRADLE_CACHE_ENABLED is `yes`     |               |
| GRADLE_CACHE_PASSWORD *     | string   | Gradle cache password to authenticate the request, required if GRADLE_CACHE_ENABLED is `yes` |               |

```groovy
iotatechSettings {
    cache {
        enabled.set(false)
    }
}
```

### 🏗 Development

#### Development guides

1. [Starting Plugin Development](https://docs.gradle.org/current/userguide/custom_plugins.html)
1. [Designing Plugins](https://docs.gradle.org/current/userguide/designing_gradle_plugins.html)
1. [Implementing Plugins](https://docs.gradle.org/current/userguide/implementing_gradle_plugins.html)
1. [Publishing Plugins](https://docs.gradle.org/current/userguide/publishing_gradle_plugins.html)

#### Local Development

If new feature should be added or plugin should be tested locally - following steps should be
performed

1. Make needed changes to the plugin
2. Update version of the plugin in `build.gradle.kts` to be

```text
<MAJOR>.<MINOR>.<PATCH>-SNAPSHOT
```

3. Build and publish plugin to local repository

```bash
./gradlew clean build publishToMavenLocal
```

4. Update version of the plugin in project, check `gradle.properties` file for `iotatechPluginVersion` setting

#### Dependencies Versions updates

Plugin dependencies version settings are stored in `src/main/resources/versions.properties` file.
Use it to provide need version of the plugin or library to use.

### 📆 Versioning strategy

[Semantic Versioning](https://semver.org/) rules are applied to the plugin versioning. For sure there are specific
rules that should guide through what part of the version should be updated. Below are typical scenarios:

| Version | Cases                                                                                                                                                                         |
|---------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| PATCH   | * bugfix of the existed plugins<br/>* dependency version update which is backward compatible<br/> * new settings for an existed plugin with backward compatible default value |
| MINOR   | * minor dependency update which is not backward compatible<br/>* new plugin settings which are not backward compatible<br/>* new functionality or logic of the existed plugin |
| MAJOR   | * new external plugin support added<br/>* new kotlin or java version<br/>* new spring boot or cloud version                                                                   |

💡️ Above scenarios are not a complete and definitive guide, they do not cover all possible cases. Consider discussing
each new case and updating examples.

#### Versions update

To update a version use predefined gradle tasks:

| Version | Command                                                                           |
|---------|-----------------------------------------------------------------------------------|
| PATCH   | `gradle patch` - will increment patch version by 1                                |
| MINOR   | `gradle minor` - will increment minor version by 1 and reset patch to 0           |
| MAJOR   | `gradle major` - will increment major version by 1 and reset minor and patch to 0 |

#### Versions usage

Each project that uses plugin should consider using version of it using following rules:

1. Major version should be specified
2. Minor version should be specified
3. Path version should be set as `+`

💡️ Above rules will ensure that latest stable and fixed version of the plugin is used and will reduce cases when
plugin versions should be updated in multiple places

### 🌳 Branching Strategy

Main development of the plugin will be done in a `master` branch, so all modification for patch, minor and major versions
will be merged directly to `master` branch.

#### 🆕 Working on a new change

When added new functionality or fixing the latest version of the plugin follow these steps:

1. Create new `feature` or `bugfix` branch from the latest version of the `master` branch
2. Make needed changes
3. Gather approves from the team 
4. Increment version of the plugin based on type of the change
5. Merge changes back to the `master` branch
6. Wait for plugin to be published and update versions in affected services if needed

#### 🐞 Working on a hotfix

There can be cases when a bug was found in old version of the plugin and this plugin version is still in use in some
not released version of the code. Such a scenario requires a hotfix: 

1. Create new `hotfix` branch from the needed tag
2. Make needed changes
3. Gather approves from the team
4. Increment version of the plugin based on type of the change
5. Publish plugin with a new version
6. Make sure that bugfix is propagated for all next versions which are in use 
7. Leave hotfix branch till it will not be needed

💡️ When incrementing version make sure that you change only patch part of the version

### 📤 Publishing plugin

Plugin will be published automatically

1. Update version of the plugin using versioning rules described above.
2. Publish plugin with a new version (CI step)
3. Create a new tag base on a version in a format `v<MAJOR>.<MINOR>.<PATCH>` (CI step)
4. Push changes to a master branch from a feature branch

```bash
./gradlew publishPlugins -Pgradle.publish.key=<key> -Pgradle.publish.secret=<secret>
```

💡️ Steps 2-3 should be done via related CI/CD job, not need to perform them manually. Check below _CI/CD_ section.

### CI/CD

Process of plugins build and deployment is handled by a teamcity jobs, details are below:

| Job                                 | Link                                                                                             | Description                                                                                                                                                                       |
|-------------------------------------|--------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Validate and build plugins          | https://teamcity.iotatech.cloud/buildConfiguration/Build_Gradle_Plugins#all-projects             | Triggers automatically when changes are merged into any branch. Includes steps to validate plugins configuration and build deployable units.                                      |
| Validate, build and publish plugins | https://teamcity.iotatech.cloud/buildConfiguration/Build_And_Publish_Gradle_Plugins#all-projects | Should be triggered manually when plugin version should be published. Includes steps from build pipeline and also creates a tag based on version and publishes plugin to a portal |

💡️ If one does not have access to described _CI/CD_ pipelines - request access from architects

💡 Same version of the plugin cannot be published more than one time, as it is forbidden by gradle plugins portal. In
case when it is required - published plugins versions can be manually removed from portal within 3-4 hours after publish.
Consult with architects in case when such an action will be required.
