package uk.co.iotatech.gradle.tasks.wrapper

import org.gradle.api.Project
import org.gradle.api.tasks.wrapper.Wrapper

fun Project.configureGradleWrapperTask(ext: IotatechGradleExtension) {
    tasks.register("gradleWrapper", Wrapper::class.java) {
        gradleVersion = ext.version.get()
        distributionUrl = "https://services.gradle.org/distributions/gradle-${gradleVersion}-bin.zip"
    }
}