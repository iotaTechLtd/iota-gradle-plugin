package uk.co.iotatech.gradle.tasks.kotlin

import org.gradle.api.Project
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

fun Project.configureCompileKotlinTask(ext: IotatechKotlinExtension) {
    tasks.withType(KotlinCompile::class.java) {
        kotlinOptions.jvmTarget = ext.jvmTarget.get().majorVersion
        kotlinOptions.freeCompilerArgs = ext.freeCompilerArgs.get()
    }
}