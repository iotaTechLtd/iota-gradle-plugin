package uk.co.iotatech.gradle.tasks.jar

import org.gradle.api.Project
import org.gradle.jvm.tasks.Jar
import org.gradle.kotlin.dsl.provideDelegate
import org.springframework.boot.gradle.tasks.bundling.BootJar

fun Project.configureJarTask(ext: IotatechJarExtension) {
    tasks.withType(Jar::class.java) {
        val MODULE_VERSION: String by project

        enabled = true
        archiveVersion.set(MODULE_VERSION)

        if (ext.archiveBaseName.isPresent) {
            archiveBaseName.set(ext.archiveBaseName.get())
        }
    }
}

fun Project.configureBootJarTask(ext: IotatechJarExtension) {
    tasks.withType(Jar::class.java) {
        val MODULE_VERSION: String by project

        enabled = false
        archiveVersion.set(MODULE_VERSION)

        if (ext.archiveBaseName.isPresent) {
            archiveBaseName.set(ext.archiveBaseName.get())
        }
    }

    tasks.withType(BootJar::class.java) {
        val MODULE_VERSION: String by project

        enabled = true
        archiveVersion.set(MODULE_VERSION)

        if (ext.archiveBaseName.isPresent) {
            archiveBaseName.set(ext.archiveBaseName.get())
        }
    }
}