package uk.co.iotatech.gradle.tasks.archive

import org.gradle.api.Project
import org.gradle.api.tasks.bundling.AbstractArchiveTask

fun Project.configureArchiveTask() {
    tasks.withType(AbstractArchiveTask::class.java) {
        isPreserveFileTimestamps = false
        isReproducibleFileOrder = true
    }
}
