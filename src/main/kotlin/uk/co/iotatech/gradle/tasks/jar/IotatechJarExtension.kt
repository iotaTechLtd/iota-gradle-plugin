package uk.co.iotatech.gradle.tasks.jar

import org.gradle.api.provider.Property

abstract class IotatechJarExtension {
    abstract val archiveBaseName: Property<String>
}