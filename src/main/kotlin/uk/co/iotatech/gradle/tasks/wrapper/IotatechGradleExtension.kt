package uk.co.iotatech.gradle.tasks.wrapper

import org.gradle.api.provider.Property
import uk.co.iotatech.gradle.GRADLE_VERSION
import uk.co.iotatech.gradle.getVersion

abstract class IotatechGradleExtension {

    abstract val version: Property<String>

    init {
        version.convention(getVersion(GRADLE_VERSION))
    }
}