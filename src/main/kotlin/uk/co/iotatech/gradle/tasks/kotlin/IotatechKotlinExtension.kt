package uk.co.iotatech.gradle.tasks.kotlin

import org.gradle.api.JavaVersion
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property

abstract class IotatechKotlinExtension {

    abstract val jvmTarget: Property<JavaVersion>
    abstract val freeCompilerArgs: ListProperty<String>

    init {
        jvmTarget.convention(JavaVersion.VERSION_17)
        freeCompilerArgs.convention(listOf())
    }
}