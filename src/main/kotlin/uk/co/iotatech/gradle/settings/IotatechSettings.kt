package uk.co.iotatech.gradle.settings

import org.gradle.api.initialization.Settings
import org.gradle.caching.http.HttpBuildCache
import java.net.URI

fun Settings.configureBuildCache(ext: IotatechCacheSettingsExtension) {
    if (ext.enabled.get()) {
        buildCache {
            if (System.getenv("GRADLE_CACHE_ENABLED") == "true") {
                remote(HttpBuildCache::class.java) {
                    url = URI(System.getenv("GRADLE_CACHE_URL"))
                    isAllowInsecureProtocol = true
                    isPush = true
                    credentials {
                        username = System.getenv("GRADLE_CACHE_USER")
                        password = System.getenv("GRADLE_CACHE_PASSWORD")
                    }
                }
            }
        }
    }
}