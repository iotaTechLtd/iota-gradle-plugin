package uk.co.iotatech.gradle.settings

import org.gradle.api.provider.Property

abstract class IotatechCacheSettingsExtension {
    abstract val enabled: Property<Boolean>

    init {
        enabled.convention(true)
    }
}