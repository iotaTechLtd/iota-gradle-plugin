package uk.co.iotatech.gradle.repositories

import org.gradle.api.Project
import org.gradle.api.credentials.AwsCredentials
import java.net.URI

fun Project.configureRepositories() {
    repositories.add(repositories.mavenLocal())
    repositories.add(repositories.maven {
        url = URI(System.getenv("ITC_MAVEN_AWS_S3_REPO_URL"))
        if (System.getenv("ITC_MAVEN_AWS_S3_REPO_USERNAME") == null || System.getenv("ITC_MAVEN_AWS_S3_REPO_USERNAME")
                .isBlank()
        ) {
            credentials(AwsCredentials::class.java) {
                accessKey = System.getenv("AWS_ACCESS_KEY_ID")
                secretKey = System.getenv("AWS_SECRET_ACCESS_KEY")
                sessionToken = System.getenv("AWS_SESSION_TOKEN")
            }
        } else {
            credentials(AwsCredentials::class.java) {
                accessKey = System.getenv("ITC_MAVEN_AWS_S3_REPO_USERNAME")
                secretKey = System.getenv("ITC_MAVEN_AWS_S3_REPO_PASSWORD")
            }
        }
    })
    repositories.add(repositories.mavenCentral())
    repositories.add(repositories.gradlePluginPortal())
}