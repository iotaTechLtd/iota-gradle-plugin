package uk.co.iotatech.gradle

import java.util.Properties

const val GRADLE_VERSION = "gradleVersion"
const val BOOT_VERSION = "bootVersion"
const val CLOUD_VERSION = "cloudVersion"
const val DETEKT_VERSION = "detektVersion"
const val KT_LINT_VERSION = "ktlintVersion"
const val JACOCO_VERSION = "jacocoVersion"
const val KOTLIN_VERSION = "kotlinVersion"

private val versions = Properties().apply {
    load(IotatechCorePlugin::class.java.classLoader.getResourceAsStream("versions.properties")!!)
}

fun getVersion(key: String): String = versions.getProperty(key)