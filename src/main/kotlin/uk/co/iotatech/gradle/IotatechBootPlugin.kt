package uk.co.iotatech.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.get
import uk.co.iotatech.gradle.extensions.IotatechPluginExtension
import uk.co.iotatech.gradle.plugins.spring.configureSpringPlugin
import uk.co.iotatech.gradle.tasks.archive.configureArchiveTask
import uk.co.iotatech.gradle.tasks.jar.configureBootJarTask

class IotatechBootPlugin : Plugin<Project> {

    override fun apply(project: Project): Unit = with(project) {
        plugins.apply("org.springframework.boot")

        afterEvaluate {
            val ext = extensions["iotatech"] as IotatechPluginExtension
            configureSpringPlugin()
            configureArchiveTask()
            configureBootJarTask(ext.getJar())
        }
    }
}
