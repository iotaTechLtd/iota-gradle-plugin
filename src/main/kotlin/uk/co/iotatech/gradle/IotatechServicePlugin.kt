package uk.co.iotatech.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

class IotatechServicePlugin : Plugin<Project> {

    override fun apply(project: Project): Unit = with(project) {
        plugins.apply("uk.co.iotatech.gradle")
        plugins.apply("uk.co.iotatech.gradle.boot")
        plugins.apply("uk.co.iotatech.gradle.ktlint")
        plugins.apply("uk.co.iotatech.gradle.detekt")
        plugins.apply("uk.co.iotatech.gradle.jacoco")
        plugins.apply("uk.co.iotatech.gradle.sonar")
        plugins.apply("uk.co.iotatech.gradle.versioning")
    }
}
