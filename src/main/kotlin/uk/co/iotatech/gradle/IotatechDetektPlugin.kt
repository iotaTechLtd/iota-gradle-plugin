package uk.co.iotatech.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.get
import uk.co.iotatech.gradle.extensions.IotatechPluginExtension
import uk.co.iotatech.gradle.plugins.detekt.configureDetektPlugin

class IotatechDetektPlugin : Plugin<Project> {

    override fun apply(project: Project): Unit = with(project) {
        plugins.apply("io.gitlab.arturbosch.detekt")

        afterEvaluate {
            val ext = extensions["iotatech"] as IotatechPluginExtension

            configureDetektPlugin(ext.getDetekt())
        }
    }
}
