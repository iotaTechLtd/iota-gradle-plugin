package uk.co.iotatech.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.get
import uk.co.iotatech.gradle.extensions.IotatechPluginExtension
import uk.co.iotatech.gradle.plugins.configuration.configureBuildConfigurations
import uk.co.iotatech.gradle.plugins.dependency.configureDependencyManagementPlugin
import uk.co.iotatech.gradle.repositories.configureRepositories
import uk.co.iotatech.gradle.tasks.archive.configureArchiveTask
import uk.co.iotatech.gradle.tasks.jar.configureJarTask
import uk.co.iotatech.gradle.tasks.kotlin.configureCompileKotlinTask
import uk.co.iotatech.gradle.tasks.wrapper.configureGradleWrapperTask

class IotatechCorePlugin : Plugin<Project> {

    override fun apply(project: Project): Unit = with(project) {
        extensions.create(
            "iotatech",
            IotatechPluginExtension::class.java
        )

        applyPlugins()
        configureRepositories()

        afterEvaluate {
            val ext = extensions["iotatech"] as IotatechPluginExtension

            configureBuildConfigurations(ext.getBuildConfigurations())
            configureDependencyManagementPlugin(ext.getDependencyManagement())
            configureCompileKotlinTask(ext.getKotlin())
            configureGradleWrapperTask(ext.getGradle())
            configureJarTask(ext.getJar())
            configureArchiveTask()
        }
    }

    private fun Project.applyPlugins() {
        plugins.apply("java")
        plugins.apply("kotlin")
        plugins.apply("kotlin-jpa")
        plugins.apply("kotlin-noarg")
        plugins.apply("kotlin-allopen")
        plugins.apply("kotlin-spring")
        plugins.apply("io.spring.dependency-management")
    }
}
