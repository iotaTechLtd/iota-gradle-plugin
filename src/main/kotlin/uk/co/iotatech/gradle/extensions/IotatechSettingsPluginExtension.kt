package uk.co.iotatech.gradle.extensions

import org.gradle.api.Action
import org.gradle.api.tasks.Nested
import uk.co.iotatech.gradle.settings.IotatechCacheSettingsExtension

abstract class IotatechSettingsPluginExtension {

    @Nested
    abstract fun getCache(): IotatechCacheSettingsExtension

    open fun cache(action: Action<in IotatechCacheSettingsExtension>) {
        action.execute(getCache())
    }
}