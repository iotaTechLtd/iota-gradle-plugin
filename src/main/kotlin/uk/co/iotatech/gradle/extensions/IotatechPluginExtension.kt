package uk.co.iotatech.gradle.extensions

import org.gradle.api.Action
import org.gradle.api.tasks.Nested
import uk.co.iotatech.gradle.plugins.configuration.IotatechBuildConfigurationsExtension
import uk.co.iotatech.gradle.plugins.dependency.IotatechDependencyManagementExtension
import uk.co.iotatech.gradle.plugins.detekt.IotatechDetektExtension
import uk.co.iotatech.gradle.plugins.jacoco.IotatechJacocoExtension
import uk.co.iotatech.gradle.plugins.ktlint.IotatechKtLintExtension
import uk.co.iotatech.gradle.plugins.publishing.IotatechPublishingExtension
import uk.co.iotatech.gradle.plugins.sonar.IotatechSonarqubePluginExtension
import uk.co.iotatech.gradle.plugins.versions.IotatechVersioningExtension
import uk.co.iotatech.gradle.tasks.jar.IotatechJarExtension
import uk.co.iotatech.gradle.tasks.kotlin.IotatechKotlinExtension
import uk.co.iotatech.gradle.tasks.wrapper.IotatechGradleExtension

abstract class IotatechPluginExtension {
    @Nested
    abstract fun getSonar(): IotatechSonarqubePluginExtension

    @Nested
    abstract fun getGradle(): IotatechGradleExtension

    @Nested
    abstract fun getKotlin(): IotatechKotlinExtension

    @Nested
    abstract fun getDetekt(): IotatechDetektExtension

    @Nested
    abstract fun getKtlint(): IotatechKtLintExtension

    @Nested
    abstract fun getJar(): IotatechJarExtension

    @Nested
    abstract fun getDependencyManagement(): IotatechDependencyManagementExtension

    @Nested
    abstract fun getVersioning(): IotatechVersioningExtension

    @Nested
    abstract fun getBuildConfigurations(): IotatechBuildConfigurationsExtension

    @Nested
    abstract fun getJacoco(): IotatechJacocoExtension

    @Nested
    abstract fun getPublishing(): IotatechPublishingExtension

    open fun sonar(action: Action<in IotatechSonarqubePluginExtension>) {
        action.execute(getSonar())
    }

    open fun gradle(action: Action<in IotatechGradleExtension>) {
        action.execute(getGradle())
    }

    open fun kotlin(action: Action<in IotatechKotlinExtension>) {
        action.execute(getKotlin())
    }

    open fun detekt(action: Action<in IotatechDetektExtension>) {
        action.execute(getDetekt())
    }

    open fun ktlint(action: Action<in IotatechKtLintExtension>) {
        action.execute(getKtlint())
    }

    open fun jar(action: Action<in IotatechJarExtension>) {
        action.execute(getJar())
    }

    open fun dependencyManagement(action: Action<in IotatechDependencyManagementExtension>) {
        action.execute(getDependencyManagement())
    }

    open fun versioning(action: Action<in IotatechVersioningExtension>) {
        action.execute(getVersioning())
    }

    open fun buildConfigurations(action: Action<in IotatechBuildConfigurationsExtension>) {
        action.execute(getBuildConfigurations())
    }

    open fun jacoco(action: Action<in IotatechJacocoExtension>) {
        action.execute(getJacoco())
    }

    open fun publishing(action: Action<in IotatechPublishingExtension>) {
        action.execute(getPublishing())
    }
}