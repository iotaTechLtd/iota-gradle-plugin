package uk.co.iotatech.gradle.plugins.sonar

import org.gradle.api.provider.Property

abstract class IotatechSonarqubePluginExtension {
    abstract val enabled: Property<Boolean>
    abstract val projectName: Property<String>
    abstract val projectKey: Property<String>

    init {
        enabled.convention(true)
    }
}