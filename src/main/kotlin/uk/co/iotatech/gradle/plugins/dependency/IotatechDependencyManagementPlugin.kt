package uk.co.iotatech.gradle.plugins.dependency

import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies

fun Project.configureDependencyManagementPlugin(ext: IotatechDependencyManagementExtension) {
    if (ext.enabled.get()) {
        val kotlinVersion = ext.kotlinVersion.get()

        extensions.configure(DependencyManagementExtension::class.java) {
            val cloudVersion = ext.getCloud().version.get()
            val bootVersion = ext.getBoot().version.get()

            imports {
                mavenBom("org.springframework.cloud:spring-cloud-dependencies:$cloudVersion") {
                    bomProperty("kotlin.version", kotlinVersion)

                    ext.getCloud().overrides.get().forEach { (dep, version) ->
                        bomProperty(dep, version)
                    }
                }
                mavenBom("org.springframework.boot:spring-boot-starter-parent:$bootVersion") {
                    bomProperty("kotlin.version", kotlinVersion)

                    ext.getBoot().overrides.get().forEach { (dep, version) ->
                        bomProperty(dep, version)
                    }
                }
            }
        }

        dependencies {
            add("implementation", "org.jetbrains.kotlin:kotlin-stdlib")
            add("implementation", "org.jetbrains.kotlin:kotlin-reflect")
        }
    }
}