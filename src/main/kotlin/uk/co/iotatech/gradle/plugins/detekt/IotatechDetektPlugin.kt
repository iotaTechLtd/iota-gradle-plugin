package uk.co.iotatech.gradle.plugins.detekt

import io.gitlab.arturbosch.detekt.extensions.DetektExtension
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies

fun Project.configureDetektPlugin(ext: IotatechDetektExtension) {
    val detektVersion = ext.version.get()

    extensions.configure(DetektExtension::class.java) {
        toolVersion = detektVersion
        source = files(ext.source.get())
        config = files(ext.config.get())
    }

    dependencies {
        add("detekt", "io.gitlab.arturbosch.detekt:detekt-cli:$detektVersion")
    }
}