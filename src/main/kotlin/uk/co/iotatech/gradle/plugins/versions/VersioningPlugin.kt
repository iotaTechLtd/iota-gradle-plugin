package uk.co.iotatech.gradle.plugins.versions

import org.gradle.api.Project
import org.gradle.kotlin.dsl.provideDelegate
import org.gradle.kotlin.dsl.withGroovyBuilder

fun Project.configureVersioningPlugin(ext: IotatechVersioningExtension) {
    if (ext.enabled.get()) {
        val prj = this
        tasks.create("increaseModuleBuildVersion") {
            doLast {
                val MODULE_VERSION: String by prj
                val buildNumber: String by prj

                val moduleVersions = MODULE_VERSION.split(".").toMutableList()
                val buildNumberPosition = moduleVersions.size - 1
                moduleVersions[buildNumberPosition] = buildNumber
                val newModuleVersion = moduleVersions.joinToString(separator = ".")

                ant.withGroovyBuilder {
                    "propertyfile"("file" to "gradle.properties") {
                        "entry"("key" to "MODULE_VERSION", "value" to newModuleVersion)
                    }
                }
            }
        }

        tasks.create("increaseModuleMinorVersion") {
            doLast {
                val MODULE_VERSION: String by prj
                val buildNumber: String by prj

                val moduleVersions = MODULE_VERSION.split(".").toMutableList()
                val minorVersionPosition = moduleVersions.size - 2
                val previousMinorVersion = Integer.parseInt(moduleVersions[minorVersionPosition])

                if (project.hasProperty("buildNumber")) {
                    val buildNumberPosition = moduleVersions.size - 1
                    moduleVersions[buildNumberPosition] = buildNumber
                }
                moduleVersions[minorVersionPosition] = Integer.toString(previousMinorVersion + 1)
                val newModuleVersion = moduleVersions.joinToString(separator = ".")

                ant.withGroovyBuilder {
                    "propertyfile"("file" to "gradle.properties") {
                        "entry"("key" to "MODULE_VERSION", "value" to newModuleVersion)
                    }
                }
            }
        }

        tasks.create("increaseModuleMajorVersion") {
            doLast {
                val MODULE_VERSION: String by prj
                val buildNumber: String by prj

                val moduleVersions = MODULE_VERSION.split(".").toMutableList()

                val minorVersionPosition = moduleVersions.size - 2
                val majorVersionPosition = moduleVersions.size - 3
                val previousMajorVersion = Integer.parseInt(moduleVersions[majorVersionPosition])

                if (project.hasProperty("buildNumber")) {
                    val buildNumberPosition = moduleVersions.size - 1
                    moduleVersions[buildNumberPosition] = buildNumber
                }
                moduleVersions[minorVersionPosition] = "0"
                moduleVersions[majorVersionPosition] = Integer.toString(previousMajorVersion + 1)
                val newModuleVersion = moduleVersions.joinToString(separator = ".")

                ant.withGroovyBuilder {
                    "propertyfile"("file" to "gradle.properties") {
                        "entry"("key" to "MODULE_VERSION", "value" to newModuleVersion)
                    }
                }
            }
        }

    }
}