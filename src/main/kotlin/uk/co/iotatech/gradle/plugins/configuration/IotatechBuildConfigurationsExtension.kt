package uk.co.iotatech.gradle.plugins.configuration

import org.gradle.api.provider.Property

abstract class IotatechBuildConfigurationsExtension {
    abstract val enabled: Property<Boolean>
    abstract val cacheTtl: Property<Int>
    abstract val cacheUnit: Property<String>

    init {
        enabled.convention(true)
        cacheTtl.convention(4)
        cacheUnit.convention("hours")
    }
}