package uk.co.iotatech.gradle.plugins.dependency

import org.gradle.api.Action
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Nested
import uk.co.iotatech.gradle.BOOT_VERSION
import uk.co.iotatech.gradle.CLOUD_VERSION
import uk.co.iotatech.gradle.KOTLIN_VERSION
import uk.co.iotatech.gradle.getVersion

abstract class IotatechDependencyManagementExtension {
    abstract val enabled: Property<Boolean>

    abstract val kotlinVersion: Property<String>

    @Nested
    abstract fun getBoot(): BootIotatechDependencyDOM

    @Nested
    abstract fun getCloud(): CloudIotatechDependencyDOM

    init {
        enabled.convention(true)
        kotlinVersion.convention(getVersion(KOTLIN_VERSION))
    }

    open fun boot(action: Action<in BootIotatechDependencyDOM>) {
        action.execute(getBoot())
    }

    open fun cloud(action: Action<in CloudIotatechDependencyDOM>) {
        action.execute(getCloud())
    }

    abstract class BootIotatechDependencyDOM : IotatechDependencyDOM() {
        init {
            version.convention(getVersion(BOOT_VERSION))
        }
    }

    abstract class CloudIotatechDependencyDOM : IotatechDependencyDOM() {
        init {
            version.convention(getVersion(CLOUD_VERSION))
        }
    }

    abstract class IotatechDependencyDOM {
        abstract val version: Property<String>
        abstract val overrides: MapProperty<String, String>

        init {
            overrides.convention(mapOf())
        }
    }
}