package uk.co.iotatech.gradle.plugins.sonar

import org.gradle.api.Project
import org.sonarqube.gradle.SonarQubeExtension

fun Project.configureSonarPlugin(ext: IotatechSonarqubePluginExtension) {
    if (ext.enabled.get()) {
        extensions.configure(SonarQubeExtension::class.java) {
            properties {
                property("sonar.projectName", ext.projectName.get())
                property("sonar.projectKey", ext.projectKey.get())
                property("sonar.java.binaries", "build/classes")
                property("detekt.sonar.kotlin.config.path", "${projectDir}/detekt.yml")
                property("detekt.sonar.kotlin.filters", "**/resources/**,**/build/**,**/test/**")
                property(
                    "sonar.coverage.jacoco.xmlReportPaths",
                    "${buildDir}/reports/jacoco/test/jacocoTestReport.xml"
                )
                property("sonar.kotlin.detekt.reportPaths", "${buildDir}/reports/detekt/generated.xml")
                property(
                    "sonar.coverage.exclusions",
                    "**/model/**/*.*,**/dto/**/*.*,**/*Dto.*,**/*Configuration.*,**/*Config.*," +
                        "**/*Constant*.*,**/*Test.*,**/*Fallback*.*,**/**Application*.*,**/security/**,**/**Sink.*,**/**Source.*," +
                        "**/liquibase/**,**/**Properties.*,**/*Controller.*,**/activation/**"
                )
            }
        }
    }
}