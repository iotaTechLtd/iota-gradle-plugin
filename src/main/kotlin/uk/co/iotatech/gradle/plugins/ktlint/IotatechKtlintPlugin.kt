package uk.co.iotatech.gradle.plugins.ktlint

import org.gradle.api.Project
import org.gradle.api.tasks.JavaExec
import org.gradle.kotlin.dsl.get

fun Project.configureKtlintPlugin(ext: IotatechKtLintExtension) {
    if (ext.enabled.get()) {
        configurations.create("ktlint")
        dependencies.add("ktlint", "com.pinterest:ktlint:${ext.version.get()}")

        tasks.register("ktlint", JavaExec::class.java) {
            group = "verification"

            description = "Check Kotlin code style."
            mainClass.set("com.pinterest.ktlint.Main")
            classpath = configurations["ktlint"]
            args = listOf("!**/generated/**", "!**/test/**", "**/*.kt")
        }

        tasks.register("formatKotlin", JavaExec::class.java) {
            description = "Fix Kotlin code style deviations."
            mainClass.set("com.pinterest.ktlint.Main")
            classpath = configurations["ktlint"]
            args = listOf("-F", "!**/generated/**", "!**/test/**", "**/*.kt")
        }

        tasks.getByName("check").dependsOn(tasks.getByName("ktlint"))
    }
}