package uk.co.iotatech.gradle.plugins.detekt

import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import uk.co.iotatech.gradle.DETEKT_VERSION
import uk.co.iotatech.gradle.getVersion

abstract class IotatechDetektExtension {
    abstract val version: Property<String>
    abstract val source: ListProperty<String>
    abstract val config: Property<String>

    init {
        version.convention(getVersion(DETEKT_VERSION))
        source.convention(listOf("src/main"))
        config.convention("detekt.yml")
    }
}