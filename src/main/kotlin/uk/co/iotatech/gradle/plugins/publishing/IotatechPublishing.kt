package uk.co.iotatech.gradle.plugins.publishing

import org.gradle.api.Project
import org.gradle.api.credentials.AwsCredentials
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.kotlin.dsl.provideDelegate
import org.gradle.kotlin.dsl.withGroovyBuilder
import java.net.URI

fun Project.configurePublishing(ext: IotatechPublishingExtension) {
    val s3PublishUrl = System.getenv("ITC_MAVEN_AWS_S3_REPO_PUBLISH_URL")
    if (ext.enabled.get() && s3PublishUrl != null) {
        extensions.configure(PublishingExtension::class.java) {
            repositories {
                maven {
                    url = URI(s3PublishUrl)
                    if (System.getenv("ITC_MAVEN_AWS_S3_REPO_USERNAME").isNullOrBlank()) {
                        credentials(AwsCredentials::class.java) {
                            accessKey = System.getenv("AWS_ACCESS_KEY_ID")
                            secretKey = System.getenv("AWS_SECRET_ACCESS_KEY")
                            sessionToken = System.getenv("AWS_SESSION_TOKEN")
                        }
                    } else {
                        credentials(AwsCredentials::class.java) {
                            accessKey = System.getenv("ITC_MAVEN_AWS_S3_REPO_USERNAME")
                            secretKey = System.getenv("ITC_MAVEN_AWS_S3_REPO_PASSWORD")
                        }
                    }
                }
            }

            publications {
                val MODULE_VERSION: String by project

                create("maven", MavenPublication::class.java) {
                    groupId = rootProject.group as String
                    artifactId = project.name
                    version = MODULE_VERSION

                    from(components.getByName("java"))
                }
            }
        }
    }

    if (ext.enabled.get()) {
        val prj = this

        tasks.create("makeModuleVersionSnapshot") {
            group = "publishing"

            doLast {
                val MODULE_VERSION: String by prj
                val versions = MODULE_VERSION.split(".").toMutableList()

                val newVersion = "${versions[0]}.${versions[1]}-SNAPSHOT"

                ant.withGroovyBuilder {
                    "propertyfile"("file" to "gradle.properties") {
                        "entry"("key" to "MODULE_VERSION", "value" to newVersion)
                    }
                }
            }
        }
    }
}
