package uk.co.iotatech.gradle.plugins.ktlint

import org.gradle.api.provider.Property
import uk.co.iotatech.gradle.KT_LINT_VERSION
import uk.co.iotatech.gradle.getVersion

abstract class IotatechKtLintExtension {
    abstract val enabled: Property<Boolean>
    abstract val version: Property<String>

    init {
        enabled.convention(true)
        version.convention(getVersion(KT_LINT_VERSION))
    }
}