package uk.co.iotatech.gradle.plugins.publishing

import org.gradle.api.provider.Property

abstract class IotatechPublishingExtension {
    abstract val enabled: Property<Boolean>

    init {
        enabled.convention(true)
    }
}
