package uk.co.iotatech.gradle.plugins.jacoco

import org.gradle.api.Action
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Nested
import uk.co.iotatech.gradle.JACOCO_VERSION
import uk.co.iotatech.gradle.getVersion

abstract class IotatechJacocoExtension {
    abstract val enabled: Property<Boolean>
    abstract val version: Property<String>
    abstract val exclusions: ListProperty<String>

    @Nested
    abstract fun getReports(): IotatechJacocoReportsExtension

    init {
        enabled.convention(true)
        version.convention(getVersion(JACOCO_VERSION))
        exclusions.convention(listOf())
    }

    open fun reports(action: Action<in IotatechJacocoReportsExtension>) {
        action.execute(getReports())
    }

    abstract class IotatechJacocoReportsExtension {
        abstract val htmlEnabled: Property<Boolean>
        abstract val xmlEnabled: Property<Boolean>

        init {
            htmlEnabled.convention(false)
            xmlEnabled.convention(true)
        }
    }
}