package uk.co.iotatech.gradle.plugins.versions

import org.gradle.api.provider.Property

abstract class IotatechVersioningExtension {
    abstract val enabled: Property<Boolean>

    init {
        enabled.convention(true)
    }
}