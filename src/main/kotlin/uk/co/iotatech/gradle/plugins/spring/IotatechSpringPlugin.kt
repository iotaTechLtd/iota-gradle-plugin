package uk.co.iotatech.gradle.plugins.spring

import org.gradle.api.Project
import org.springframework.boot.gradle.dsl.SpringBootExtension

fun Project.configureSpringPlugin() {
    extensions.configure(SpringBootExtension::class.java) {
        buildInfo()
    }
}
