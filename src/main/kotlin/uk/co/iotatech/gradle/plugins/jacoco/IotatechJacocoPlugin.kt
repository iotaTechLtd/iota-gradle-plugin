package uk.co.iotatech.gradle.plugins.jacoco

import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.testing.Test
import org.gradle.api.tasks.testing.TestReport
import org.gradle.testing.jacoco.plugins.JacocoPluginExtension
import org.gradle.testing.jacoco.tasks.JacocoReport

fun Project.configureJacoco(ext: IotatechJacocoExtension) {
    if (ext.enabled.get()) {
        configurePlugin(ext)
        configureTasks(ext)
    }
}

private fun Project.configureTasks(ext: IotatechJacocoExtension) {
    tasks.create("testReport", TestReport::class.java) {
        afterEvaluate {
            destinationDir = file("$buildDir/reports/tests/test")
            reportOn(tasks.withType(Test::class.java))
        }
    }

    tasks.create("combineJacocoReports", JacocoReport::class.java) {
        reports {
            html.required.set(ext.getReports().htmlEnabled.get())
            xml.required.set(ext.getReports().xmlEnabled.get())

            html.outputLocation.set(layout.buildDirectory.dir("reports/jacoco/test/html"))
            xml.outputLocation.set(layout.buildDirectory.file("reports/jacoco/test/jacocoTestReport.xml"))
        }

        afterEvaluate {
            dependsOn(tasks.getByName("test"), tasks.getByName("testReport"))
            executionData(fileTree(buildDir.absolutePath).include("jacoco/*.exec"))

            val javaPluginExtension = project.extensions.getByType(JavaPluginExtension::class.java)
            val mainSourceSet = javaPluginExtension.sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME)

            classDirectories.setFrom(files(mainSourceSet.output))
            sourceDirectories.setFrom(files(mainSourceSet.allSource.srcDirs))
        }

        afterEvaluate {
            classDirectories.setFrom(files(classDirectories.files.map {
                fileTree(mapOf(
                    "dir" to it,
                    "exclude" to mutableListOf(
                        "**/model/**/*.*",
                        "**/dto/**/*.*",
                        "**/**Dto.*",
                        "**/**Configuration.*",
                        "**/**Config.*",
                        "**/**Test.*",
                        "**/**Constant*.*",
                        "**/**Constants*.*",
                        "**/**FallbackFactory*.*",
                        "**/**Fallback*.*",
                        "**/**Application*.*",
                        "**/security/**",
                        "**/**Sink.*",
                        "**/**Source.*",
                        "**/liquibase/**",
                        "**/**Properties.*",
                        "**/**Controller.*",
                        "**/activation/**",
                    ).plus(ext.exclusions.get())
                ))
            }))
        }
    }

    tasks.getByName("test")
        .finalizedBy(tasks.getByName("testReport"))
    tasks.getByName("test")
        .finalizedBy(tasks.getByName("combineJacocoReports"))
    tasks.getByName("combineJacocoReports")
        .dependsOn(tasks.getByName("test"))
}

private fun Project.configurePlugin(ext: IotatechJacocoExtension) {
    extensions.configure(JacocoPluginExtension::class.java) {
        toolVersion = ext.version.get()
    }
}