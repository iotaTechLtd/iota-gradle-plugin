package uk.co.iotatech.gradle.plugins.configuration

import org.gradle.api.Project

fun Project.configureBuildConfigurations(ext: IotatechBuildConfigurationsExtension) {
    if (ext.enabled.get()) {
        configurations.all {
            resolutionStrategy.cacheDynamicVersionsFor(ext.cacheTtl.get(), ext.cacheUnit.get())
            resolutionStrategy.cacheChangingModulesFor(ext.cacheTtl.get(), ext.cacheUnit.get())

            exclude(
                mapOf(
                    "module" to "spring-boot-starter-tomcat"
                )
            )
        }
    }
}