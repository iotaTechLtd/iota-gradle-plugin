package uk.co.iotatech.gradle

import org.gradle.api.Plugin
import org.gradle.api.initialization.Settings
import org.gradle.kotlin.dsl.get
import uk.co.iotatech.gradle.extensions.IotatechSettingsPluginExtension
import uk.co.iotatech.gradle.settings.configureBuildCache

class IotatechSettingsPlugin : Plugin<Settings> {

    override fun apply(settings: Settings): Unit = with(settings) {
        extensions.create(
            "iotatechSettings",
            IotatechSettingsPluginExtension::class.java
        )

        val ext = extensions["iotatechSettings"] as IotatechSettingsPluginExtension
        configureBuildCache(ext.getCache())
    }
}