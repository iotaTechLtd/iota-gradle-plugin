package uk.co.iotatech.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.get
import uk.co.iotatech.gradle.extensions.IotatechPluginExtension
import uk.co.iotatech.gradle.plugins.versions.configureVersioningPlugin

class IotatechVersioningPlugin : Plugin<Project> {

    override fun apply(project: Project): Unit = with(project) {
        afterEvaluate {
            val ext = extensions["iotatech"] as IotatechPluginExtension

            configureVersioningPlugin(ext.getVersioning())
        }
    }
}